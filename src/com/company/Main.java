package com.company;
import java.util.Scanner;
public class Main {

    public static void main(String[] args) {
        int number;
        Scanner input = new Scanner(System.in);
        System.out.println("輸入一數，判斷其為奇數或偶數:");
        number = input.nextInt();
        if(number%2==0)
            System.out.printf("%d為偶數",number);
        else System.out.printf("%d為奇數",number);
    }
}